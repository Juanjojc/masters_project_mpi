#
#Whole x_gamma range

BEGIN PLOT /ZEUS_1997_I450085/d01-x01-y01
Title= Whole $x_{\gamma}^{OBS}$ range and $E_T^{min} = 6$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d02-x01-y01
Title= Whole $x_{\gamma}^{OBS}$ range and $E_T^{min} = 8$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d03-x01-y01
Title= Whole $x_{\gamma}^{OBS}$ range and $E_T^{min} = 11$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.025
LegendYPos=0.95
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d04-x01-y01
Title= Whole $x_{\gamma}^{OBS}$ range and $E_T^{min} = 15$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT



#
#High x_gamma range

BEGIN PLOT /ZEUS_1997_I450085/d21-x01-y01
Title= $x_{\gamma}^{OBS} \geq 0.75$ and $E_T^{min} = 6$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d22-x01-y01
Title= $x_{\gamma}^{OBS} \geq 0.75$ and $E_T^{min} = 8$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d23-x01-y01
Title= $x_{\gamma}^{OBS} \geq 0.75$ and $E_T^{min} = 11$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.05
LegendYPos=0.55
END PLOT

BEGIN PLOT /ZEUS_1997_I450085/d24-x01-y01
Title= $x_{\gamma}^{OBS} \geq 0.75$ and $E_T^{min} = 15$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.05
LegendYPos=0.9
END PLOT



#
#Low x_gamma range


BEGIN PLOT /ZEUS_1997_I450085/d25-x01-y01
Title= $0.30 < x_{\gamma}^{OBS} < 0.75$ and $E_T^{min} = 6$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT


BEGIN PLOT /ZEUS_1997_I450085/d26-x01-y01
Title= $0.30 < x_{\gamma}^{OBS} < 0.75$ and $E_T^{min} = 8$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT


BEGIN PLOT /ZEUS_1997_I450085/d27-x01-y01
Title= $0.30 < x_{\gamma}^{OBS} < 0.75$ and $E_T^{min} = 11$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT


BEGIN PLOT /ZEUS_1997_I450085/d28-x01-y01
Title= $0.30 < x_{\gamma}^{OBS} < 0.75$ and $E_T^{min} = 15$ GeV
XLabel=$\bar{\eta}$
YLabel=$\text{d}\sigma / \text{d} \bar{\eta}$ [nb]
LegendXPos=0.4
LegendYPos=0.55
END PLOT